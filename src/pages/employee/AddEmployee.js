import React,{Component} from 'react'
import { Modal,  Text, TouchableHighlight, View, Alert,StyleSheet, TouchableOpacity, Button,Picker, AsyncStorage } from 'react-native'
import { HelperText, TextInput } from 'react-native-paper';
import URLAPI from "../../config/api.config.json"
import axios from 'axios'

export default class AddEmployee extends Component{
    constructor(props){
        super(props)
        this.state={
            formdata:{
                id:'',
                code:'',
                firstName:'',
                lastName:'',
                mCompanyId:{},
                email:'',
                createdBy:'UmBlew',
                isDelete:false,
                status: 'Active'
            },
            data: [],
            pickerCompany:'',
            mCompany:[]
        }
        this.changeHandler = this.changeHandler.bind(this)
        this.submitHandler = this.submitHandler.bind(this)
        this.pilihPicker = this.pilihPicker.bind(this) 
    }

    componentDidMount(){
        let tmp = this.state.formdata
        let value = AsyncStorage.getItem("session")
        tmp['createdBy'] = JSON.parse(value.username)
        this.setState({
            formdata: tmp
        })
        alert(JSON.stringify(tmp))
    }

    componentWillReceiveProps(PROP){
        this.setState({mCompany:PROP.mCompany})
        
    }

    changeHandler(name,value){
        let tmp=this.state.formdata
        tmp[name]=value        
        this.setState({
            formdata:tmp
        })
    }

    submitHandler(){
        //let token=localStorage.getItem(apiconfig.LS.TOKEN)
            
            //alert(JSON.stringify(datas))
            const opsi = {
                url: URLAPI.BASE_URL+URLAPI.ENDPOINTS.EMPLOYEE,
                method: "post",
                headers:{
                    //"Authorization": token,
                    "Content-Type" : "application/json"
                },
                data: this.state.formdata
            } 
            //alert(JSON.stringify(this.state.formdata))
            axios(opsi)
                .then((response)=>{
                    if(response.data.status===1){
                        this.props.refresh();
                        this.props.closeModalHandler()
                    } else {
                        alert(response.status)
                    }
                })
                .catch((error)=>{ pickerCompany
                    console.log(error);            
                })
            
            
        //alert('Success')
    }

    pilihPicker(val){
        this.state.mCompany.map((row)=>{
            if(row.id===val){
                //this.state.formdata.mCompanyId=row
                this.state.pickerCompany=row.id
                alert(JSON.stringify(row)+',============ '+JSON.stringify(this.state.formdata)+'--------------'+this.state.pickerCompany)
            }}
            
        )
    }
    render(){

        //alert(JSON.stringify(this.state.mCompany))
        
        return(
                <Modal
                    animationType='slide'
                    transparent={false}
                    visible={this.props.add}
                    onRequestClose={() => {
                        this.props.close();
                    }}
                    onShow={()=> {
                        //alert(JSON.stringify(this.state.mCompany))
                    }}
                    >
                    
                        <Text>Add</Text>

                            <TextInput
                                label="First Name"
                                value={this.state.formdata.firstName}
                                onChangeText={text => this.changeHandler('firstName',text)}
                            />
                            <HelperText
                                type="error"
                                visible={!this.state.formdata.firstName}
                            >
                                First Name is invalid
                            </HelperText>
                            <TextInput
                                label="Last Name"
                                value={this.state.formdata.lastName}
                                onChangeText={text => this.changeHandler('lastName',text)}
                            />
                            <HelperText
                                type="error"
                                visible={!this.state.formdata.lastName}
                            >
                                Last Name is invalid
                            </HelperText>
                            {/* <Autocomplete
                                data={this.state.mCompanyList}
                                defaultValue={this.state.autoCompleteCompany}
                                onChangeText={text => this.setState({ autoCompleteCompany: text })}
                                placeholder="ISI COMPANY"
                                
                                style={styles.container}
                            /> */}
                            <Picker
                                selectedValue={this.state.formdata.mCompanyId?this.state.formdata.mCompanyId.id:2}
                                mode="dropdown"
                                onValueChange={(val)=>{
                                    this.state.mCompany.map((row,x)=>{
                                        if(row.id===val){
                                            let tmp = this.state.formdata 
                                            tmp.mCompanyId=row
                                            this.setState({
                                                formdata:tmp
                                            })
                                            //alert(JSON.stringify(this.state.formdata.mCompanyId.id)+',============ '+JSON.stringify(this.state.formdata)+'--------------'+this.state.pickerCompany)
                                        }}
                                    )
                                    }                                   
                                }>
                                    <Picker.Item label='-- PILIH --' value='' key={-1} />
                                {
                                    this.state.mCompany.map((row,x)=>
                                        <Picker.Item label={row.name} value={row.id} key={x} />
                                    )
                                }
                                
                            </Picker>
                            {/* <TextInput
                                label="Company"
                                value={this.state.formdata.mCompanyId}
                                onChangeText={text => this.changeHandler('mCompanyId',text)}
                            /> */}
                            {/* <HelperText
                                type="error"
                                visible={!this.state.formdata.mCompanyId}
                            >
                                Company is invalid
                            </HelperText> */}
                            <TextInput
                                label="Email"
                                value={this.state.formdata.email}
                                onChangeText={text => this.changeHandler('email',text)}
                            />
                            <HelperText
                                type="error"
                                visible={!this.state.formdata.email.includes('@')}
                            >
                                Email address is invalid!
                            </HelperText>
                        <View style={{flexDirection:'row',justifyContent:'flex-start'}}>
                        <Button
                            title="BATAL"
                            onPress={() => {
                                this.props.close();
                            }}
                            style={{backgroundColor:'red',flex:1,textAlign:'center',paddingVertical:30}}
                            >
                        </Button>
                        <Button
                            title="SIMPAN"
                            onPress={() => {
                                this.submitHandler();
                                this.props.close();
                            }}
                            style={{justifyContent:'flex-end',flex:1,color:'white',textAlign:'center',paddingVertical:30}}
                            >
                        </Button>
                        </View>
                </Modal>
        )
    }
}
const styles = StyleSheet.create({
    btn:{
        padding:5,
        backgroundColor: 'red',
        alignItems: 'center'
    },
    container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
    paddingVertical: 25
  },
})
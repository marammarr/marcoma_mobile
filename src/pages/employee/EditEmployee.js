import React,{Component} from 'react'
import { Modal,  Text, TouchableHighlight, View, Alert,StyleSheet,ScrollView, Button, Picker } from 'react-native'
import { HelperText, TextInput } from 'react-native-paper';
import URLAPI from "../../config/api.config.json"
import axios from 'axios'

export default class EditEmployee extends Component{
    constructor(props){
        super(props)
        this.state={
            formdata:{
                id:'',
                code:'',
                firstName:'',
                lastName:'',
                mCompanyId:{id:''},
                email:''
            },
            mCompany:[]
        }
        this.changeHandler = this.changeHandler.bind(this)
        this.submitHandler = this.submitHandler.bind(this)
    }

    componentWillReceiveProps(prop){
        this.setState({
            formdata:prop.data,
            mCompany:prop.mCompany
        })
        //alert(JSON.stringify(this.state.formdata))
    }

    changeHandler(name,value){
            let tmp=this.state.formdata
            tmp[name]=value        
            this.setState({
                formdata:tmp
            })
    }

    submitHandler(){
        //let token=localStorage.getItem(apiconfig.LS.TOKEN)
            
            //alert(JSON.stringify(datas))
            const opsi = {
                url: URLAPI.BASE_URL+URLAPI.ENDPOINTS.EMPLOYEE,
                method: "put",
                headers:{
                    //"Authorization": token,
                    "Content-Type" : "application/json"
                },
                data: this.state.formdata
            }
            axios(opsi)
                .then((response)=>{
                    if(response.data.status===1){
                        this.props.refresh();
                        this.props.close()
                    } else {
                        alert(response.status)
                    }
                })
                .catch((error)=>{
                    console.log(error);            
                })
            
            
        alert('Success')
    }

    render(){
        return(
            <View>
                <Modal
                    animationType='slide'
                    transparent={false}
                    visible={this.props.edit}
                    onRequestClose={() => {
                        this.props.edit=!this.props.edit
                }}>

                    <View style={{marginTop: 22}}>
                        <View>
                        <Text>Edit</Text>

                        <View>
                            <TextInput
                                label="ID Number"
                                value={this.state.formdata.id+''}
                                onChangeText={text => text}
                                disabled
                                style={{marginBottom:20}}
                            />
                        </View>
                        <View>
                            <TextInput
                                label="First Name"
                                value={this.state.formdata.firstName}
                                onChangeText={text => this.changeHandler('firstName',text)}
                            />
                            <HelperText
                                type="error"
                                visible={!this.state.formdata.id}
                            >
                                First Name is invalid
                            </HelperText>
                        </View>
                        <View>
                            <TextInput
                                label="Last Name"
                                value={this.state.formdata.lastName}
                                onChangeText={text => this.changeHandler('lastName',text)}
                            />
                            <HelperText
                                type="error"
                                visible={!this.state.formdata.lastName}
                            >
                                Last Name is invalid
                            </HelperText>
                        </View>
                        <View>
                            <Picker
                                selectedValue={this.state.formdata.mCompanyId?this.state.formdata.mCompanyId.id:2}
                                mode="dropdown"
                                onValueChange={(val)=>{
                                    this.state.mCompany.map((row,x)=>{
                                        if(row.id===val){
                                            let tmp = this.state.formdata 
                                            tmp.mCompanyId=row
                                            this.setState({
                                                formdata:tmp
                                            })
                                            //alert(JSON.stringify(this.state.formdata.mCompanyId.id)+',============ '+JSON.stringify(this.state.formdata)+'--------------'+this.state.pickerCompany)
                                        }}
                                    )
                                    }                                   
                                }>
                                    <Picker.Item label='-- PILIH --' value='' key={-1} />
                                {
                                    this.state.mCompany.map((row,x)=>
                                        <Picker.Item label={row.name} value={row.id} key={x} />
                                    )
                                }
                                
                            </Picker>
                        </View>
                        <View>
                            <TextInput
                                label="Email"
                                value={this.state.formdata.email}
                                onChangeText={text => this.changeHandler('email',text)}
                            />
                            {/* <HelperText
                                type="error"
                                visible={!this.state.formdata.email.includes('@')}
                            >
                                Email address is invalid!
                            </HelperText> */}
                        </View>
                        
                        <View style={{marginTop:40}}>
                        <Button
                            title="SIMPAN"
                            onPress={() => {
                                this.submitHandler();
                            }}
                            style={styles.btn}>
                        </Button>
                        </View>
                        <View style={{marginTop:40}}>
                        <Button
                            title="BATAL"
                            onPress={() => {
                                this.props.close();
                            }}
                            style={{marginTop:40}}
                            >
                        </Button>
                        </View>
                        
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    btn:{
        padding:5,
        backgroundColor: 'red',
        alignItems: 'center'
    }
})
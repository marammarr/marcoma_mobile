import React,{Component} from 'react'
import { Modal,  Text, TouchableHighlight, View, Alert } from 'react-native'
import { HelperText, TextInput } from 'react-native-paper';
import Styles from '../../config/style.config'

export default class ViewEmployee extends Component{
    constructor(props){
        super(props)
        this.state={
            formdata:{
                id:'',
                code:'',
                firstName:'',
                lastName:'',
                mCompanyId:{id:''},
                email:''
            }
        }

    }
    componentWillReceiveProps(newProps) {
        /* alert(JSON.stringify(newProps)) */
        this.setState({
            formdata : newProps.data
        })
    }

    render(){
        return(
            <View>
                <Modal
                    animationType='slide'
                    transparent={false}
                    visible={this.props.view}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                      }}>

                    <View style={{marginTop: 22}}>

                        <Text style={{textAlign:'center',marginBottom:20}}>View</Text>

                            <TextInput
                                keyboardType='numeric'
                                label="ID Number"
                                value={this.state.formdata.id+''}
                                onChangeText={text => text}
                                disabled
                                style={{marginBottom:20}}
                            />
                            <TextInput
                                label="First Name"
                                value={this.state.formdata.firstName}
                                /* onChangeText={text => this.setState({ text })} */
                                disabled
                                style={{marginBottom:20}}
                            />
                            <TextInput
                                label="Last Name"
                                value={this.state.formdata.lastName}
                                disabled
                                style={{marginBottom:20}}
                            />
                            <TextInput
                                label="Company"
                                value={this.state.formdata.mCompanyId?this.state.formdata.mCompanyId.name+'':''}
                                disabled
                                style={{marginBottom:20}}
                            />
                            <TextInput
                                label="Email"
                                value={this.state.formdata.email}
                                onChangeText={text => this.setState({ text })}
                                disabled
                            />
                    </View> 
                    <View style={{textAlign:'right',paddingVertical:30}}>
                        <TouchableHighlight
                            onPress={() => {
                                this.props.close();
                            }}
                            style={Styles.btn}>
                            <Text style={{color:'white',marginVertical:20,textAlign:'center'}}>CLOSE</Text>
                        </TouchableHighlight>
                    </View>
                    
                </Modal>
            </View>
        )
    }
}
import React,{Component} from 'react'
import {DataTable,Text,Button, Snackbar,Searchbar } from 'react-native-paper'
import PropTypes from 'prop-types';
import { ScrollView, StyleSheet,TouchableHighlight,View,Alert } from 'react-native';
import { Picker} from 'native-base';
import URLAPI from "../../config/api.config.json"
import axios from 'axios'
import AddEmployee from './AddEmployee'
import EditEmployee from './EditEmployee'
import ViewEmployee from './ViewEmployee'
import {Icon} from 'native-base'
import {Styles} from '../../config/style.config'

export default class ListEmployee extends Component{

    constructor(props){
        super(props)
        this.state={
            searchTeks:'',
            data:[],
            loading: true,
            currentData:{},  // Naro data yg dipilih untuk fungsi edit, hapus, view
            //parameter boolean untuk memunculkan modal
            edit:false,
            view:false,
            add:false, 
             //Untuk datatable
            page : 1,
            sorting: null,

            //Untuk pemberitahuan snackbar
            snackVisible: 'Hide',
            snackTeks: '',
            snackDurasi: 4000,

            //Untuk data mCompany
            mCompany: [],
        }

       

        //Untuk mendaftarkan fungsi
        this.getListData = this.getListData.bind(this)
        this.showNotif = this.showNotif.bind(this)
        this.editModalHandler = this.editModalHandler.bind(this)
        this.viewModalHandler = this.viewModalHandler.bind(this)
        this.closeModalHandler = this.closeModalHandler.bind(this)
        this.showHandler = this.showHandler.bind(this)
        this.deleteHandler = this.deleteHandler.bind(this)
        this.optionHandler = this.optionHandler.bind(this)
        this.searchData = this.searchData.bind(this)
    }

    componentDidMount(){    // Fungsi yg ditaro disini akan dijalankan ketika halaman di load
        this.getListData();
    }
    
    getListData(){  // Fungsi mengambil data
        //alert("Mengambil data ke "+URLAPI.BASE_URL+URLAPI.ENDPOINTS.EMPLOYEE)
        axios({method:'GET',url:URLAPI.BASE_URL+URLAPI.ENDPOINTS.EMPLOYEE, headers:{
                'Content-Type':'application/json',
                'Accepted-Language':'application/json'
            }})
            .then(res => {
                //console.warn("tts "+JSON.stringify(res.data))
            /* alert("tts "+JSON.stringify(res.data)) */
                this.setState({
                    loading:false,
                    data : res.data
                })
            })
            .catch(err => {
                console.log("Error : "+err.message+", "+err.status)
                throw err;
            });
        //Ambil mCompany
        axios({method:'GET',url:URLAPI.BASE_URL+URLAPI.ENDPOINTS.COMPANY.GET, headers:{
                'Content-Type':'application/json',
                'Accepted-Language':'application/json'
            }})
            .then(res => {
                //console.warn("tts "+JSON.stringify(res.data))
            /* alert("tts "+JSON.stringify(res.data)) */
                this.setState({
                    loading:false,
                    mCompany : res.data
                })
                //alert("ambil "+URLAPI.BASE_URL+URLAPI.ENDPOINTS.COMPANY.GET+", "+JSON.stringify(res.data))
                //alert(JSON.stringify(this.state.mCompany))
            })
            .catch(err => {
                console.log("Error : "+err.message+", "+err.status)
                throw err;
            });
   }

   // Munculin Snackbar
   showNotif(teks,durasi){
       this.snackTeks=teks
       this.snackVisible='Show'
       if(durasi)
            this.snackDurasi=durasi
        else
            this.snackDurasi=Snackbar.DURATION_MEDIUM
   }

   // Handler aksi untuk pilihan pada baris datatable
   optionHandler(val,par){
       //alert('value '+val+', '+par)
       if(val!==''){
            switch(val){
                case 'deactivate': this.activate(par,false);break;
                case 'activate': this.activate(par,true);break;
                case 'edit': this.editModalHandler(par);break;
                default: this.viewModalHandler(par); 
            }
        }
   }

   //Fungsi Aktifin Atau Nonaktifin Status
   activate(id,active){
    //MASUKIN DATA YANG DIPILIH KE CURRENTDATA
    let tmp = {}
    this.state.data.map((row) => {
        if (id == row.id) {
            tmp =row
        }
    })
    this.setState({                      
        currentData : tmp
    })
    tmp.status=active?'Active':'Deactive'   //nilai status terganting nilai parameter active
    const opsi = {
        url: URLAPI.BASE_URL+URLAPI.ENDPOINTS.EMPLOYEE,
        method: "put",
        headers:{
            //"Authorization": token,
            "Content-Type" : "application/json"
        },
        data: tmp
    }
    axios(opsi)
        .then((response)=>{
            this.getListData();
            /* if(response.data.status===1){
                this.getListData();
            } else {
                
            } */
        })
        .catch((error)=>{
            console.log(error);            
        })
   }

   // Handler Modal Add Data
   showHandler(){ 
        this.setState({add:true})
    }

    //Tahap Perbaikan
   deleteHandler(data){
        Alert.alert(
            'Konfirmasi',
            'Anda yakin ingin menghapus data dengan id '+data.id+'?',
            [
                {text:'Batal',style:'cancel'},
                {text:'Hapus', onPress:()=> 
                    axios({method:'DELETE',url:URLAPI.BASE_URL+URLAPI.ENDPOINTS.EMPLOYEE, headers:{
                        'Content-Type':'application/json',
                        'Accepted-Language':'application/json'
                    }})
                    .then(res => {
                        if(res.data.status===1){
                            this.showNotif('Data successfully deactivated')
                        }
                    })
                    .catch(err => {
                        console.log("Error : "+err.message+", "+err.status)
                        throw err;
                    })
                }
            ]
        )
   }

   editModalHandler(id) { // Handler Modal EDIT
        let tmp = {}
        
        this.state.data.map((row) => {
            if (id == row.id) {
                tmp =row
            }
        })
        this.setState({                      
            currentData : tmp,
            edit : true
        })
        //alert(JSON.stringify(this.state.currentUser))
    }
    
    viewModalHandler(id) { // Handler Modal VIEW
        //alert(userId)
        let tmp = {}
        this.state.data.map((row) => {
            if (id == row.id) {
                tmp = row
            }
        })
        this.setState({
            currentData : tmp,
            view : true
        })
        //alert(JSON.stringify(this.state.currentUser))
    }

    closeModalHandler() {
        this.setState({
            view : false,
            edit : false,
            add : false    
        })
    }

    searchData(q){
        this.setState({ searchTeks: q });
        if(q){
            //this.showNotif("Nyari "+q,200)
            if(q!==''){
                let firstNames = this.state.data.filter(
                    item=>item.firstName.toLowerCase().indexOf(q.toLowerCase()) > -1
                )
                let lastNames = this.state.data.filter(
                    item=>item.lastName.toLowerCase().indexOf(q.toLowerCase()) > -1
                )
                this.state.data = Object.assign(firstNames, lastNames)
                //alert("nyari "+q+", hasil"+JSON.stringify(this.state.data))
            }else{
                this.getListData()
            }
        }else{
            this.getListData()
        }
    }

    // Fungsi render halaman (bawaan Component React)
    render(){

        const debug = []; 
        this.state.data.map((key)=>
            debug.push(typeof(key))
        )
        /* alert(JSON.stringify(debug)) */

        const data2 = this.state.data
            .slice()
            .sort(
                (item1, item2) =>
                (this.state.sorting? 
                    item1.id < item2.id //Kalo sorting true
                        : // Kalo ga
                    item2.id < item1.id)? //  
                        1:-1
            );
        //Nyebutin kolom (opsional)
        const kolom = [];
        if(this.state.data.length>0){
            for (const [key] of Object.entries(this.state.data[0])) {
                kolom.push(key)       
            }
        }

        return(
            <View >
                <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
                    <View style={{flex:0.8}}>
                        <Searchbar
                            placeholder="Search by first / last name"
                            onChangeText={query => this.searchData(query)}
                            value={this.state.searchTeks}
                        />
                    </View>
                    <View style={{flex:0.25}}>
                        <Button mode='contained' color='orange' onPress={()=>this.showHandler()} style={{alignSelf:'flex-end',flex:1,justifyContent:'center'}}>+</Button>
                    </View>
                </View>
                
            <View>
                    
                {/* <TouchableHighlight style={{backgroundColor:'orange',marginHorizontal:50,paddingVertical:40}} onPress={()=>this.showHandler()}>
                    <Icon type="FontAwesome" name="plus"/>
                </TouchableHighlight> */}
                    </View>
                <AddEmployee
                    add={this.state.add}
                    data={this.state.data}
                    refresh={this.getListData}
                    close={this.closeModalHandler}
                    showNotif={this.showNotif}
                    mCompany={this.state.mCompany}
                />
                <EditEmployee
                    edit={this.state.edit}
                    refresh={this.getListData}
                    data={this.state.currentData}
                    close={this.closeModalHandler}
                    showNotif={this.showNotif}
                    mCompany={this.state.mCompany}
                />
                <ViewEmployee
                    view={this.state.view}
                    refresh={this.getListData}
                    data={this.state.currentData}
                    close={this.closeModalHandler}
                    showNotif={this.showNotif}
                />
            <ScrollView>
            <DataTable>
                <DataTable.Header key='1'>
                    <DataTable.Title 
                        style={styles.tengah}
                        sortDirection={this.state.sorting?'ascending':'descending'}
                        onPress={()=>
                            this.setState(state=>({
                                sorting: !state.sorting
                            }))
                        }>
                            ID
                    </DataTable.Title>
                    <DataTable.Title 
                        style={styles.tengah}
                        >
                            FULLNAME
                    </DataTable.Title>
                    <DataTable.Title 
                        style={styles.tengah}
                        >
                            COMPANY
                    </DataTable.Title>
                    <DataTable.Title 
                        style={styles.tengah}>
                            STATUS
                    </DataTable.Title>
                    
                    <DataTable.Title style={styles.tengah}>
                    
                    </DataTable.Title>
                    
                </DataTable.Header>

        {data2.map((row,x) =>
          <DataTable.Row key={x}>
            <DataTable.Cell style={styles.tengah}>{row.code}</DataTable.Cell>
            <DataTable.Cell style={styles.tengah}>{row.firstName+" "+row.lastName}</DataTable.Cell>
            <DataTable.Cell style={styles.tengah}>{row.mCompanyId.name+''}</DataTable.Cell>
            <DataTable.Cell style={styles.tengah}>{row.status}</DataTable.Cell>
            {/* <DataTable.Cell style={styles.tengah}> */}
                <Picker prompt="Action" 
                    placeholder='Action' placeholderStyle={{ color: "#bfc6ea" }}
                    mode="dropdown" note={false} 
                    onValueChange={(value)=> this.optionHandler(value,row.id)}
                    selectedValue=''>
                    <Picker.Item label='Action' value='' />
                    <Picker.Item label='Edit' value='edit' />
                    <Picker.Item label='View' value='view' />
                    <Picker.Item label={row.status==='Active'?'Deactive':'Active'} value={row.status==='Active'?'deactivate':'activate'} />
                </Picker>
            {/* </DataTable.Cell> */}
          </DataTable.Row>
            )}
        

        <DataTable.Pagination
          page={1}
          numberOfPages={3}
          onPageChange={(page) => { console.log(page); }}
          label="1-2 of 1"
        />
      </DataTable>
 
          </ScrollView>
          <Snackbar visible={this.snackVisible} onDismiss={()=>this.snackVisible=false} duration={this.snackDurasi}>{this.snackTeks}</Snackbar>
          </View>
        )
    }
    
}

const styles = StyleSheet.create({
    tengah:{
        textAlign:'center',
        justifyContent: "center",alignItems: "center"
    },
    button:{
        backgroundColor: 'lightblue'
    },
    container: {
      flex: 1,
    },
  
    content: {
      padding: 8,
    },
  
    first: {
      flex: 2,
    },
  });
import {StyleSheet} from 'react-native';

const Styles = StyleSheet.create({
    sub_btn:{
        width: 40,
        height:40,
        borderRadius: 80,
        borderWidth: 2,
        borderColor: 'black',
        borderStyle: 'solid',
        alignItems: 'center',
        justifyContent: 'center'
    },
    btn:{
        backgroundColor: '#ff6200',
    }
})

export default Styles;
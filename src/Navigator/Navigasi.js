import {NavigationService,createSwitchNavigator,createStackNavigator,createAppContainer,createBottomTabNavigator} from "react-navigation";
import React,{component} from 'react-native' 
import Icon from 'react-native-vector-icons/FontAwesome5';
import ListEmployee from '../pages/employee/ListEmployee'
import EditEmployee from '../pages/employee/EditEmployee'
import AddEmployee from '../pages/employee/AddEmployee'

const stackMenu = createStackNavigator(
    {
      
      Employee: ListEmployee
    },
    {
      initialRouteName: 'Employee'
    }
  )


const Navigasi = createAppContainer(stackMenu)
export default Navigasi 
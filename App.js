/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
//import {Platform, StyleSheet, Text, View} from 'react-native';
//import {StackMahasiswa,NavBot} from "./src/Navigator/RootStack"
/* import Login from './src/Login'
import {NavigationActions} from 'react-navigation'*/
import { Root,Text, View } from 'native-base';
import { createStackNavigator, createAppContainer,createSwitchNavigator, createMaterialTopTabNavigator} from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs'
import ListEmployee from './src/pages/employee/ListEmployee'
import EditEmployee from './src/pages/employee/EditEmployee'
import AddEmployee from './src/pages/employee/AddEmployee'
import Navigasi from './src/Navigator/Navigasi'

class App extends Component {


  render() {
    
    return (
      <View>
        <Text style={{textAlign:'center'}}>TES TABLE</Text>
        {/* <Navigasi /> */}
        <ListEmployee />
      </View>
      
    )
  }
}
export default Navigasi
